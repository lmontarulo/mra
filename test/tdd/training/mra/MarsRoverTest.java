package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class MarsRoverTest {

	@Test
	public void testMarsrover() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals(true,rover.planetContainsObstacleAt(4,7));
	}

	
	@Test
	public void executecomandTest() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,0,N)",rover.executeCommand(" "));
	}
	
	@Test
	public void executecomandTestturningright() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void executecomandTestturningleft() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void executecomandTestturningmoveon() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.RoverCaradd(7, 6, 'N');
		assertEquals("(7,7,N)",rover.executeCommand("f"));
	}
	@Test
	public void executecomandTestturningmoveback() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.RoverCaradd(5, 8, 'E');
		assertEquals("(4,8,E)",rover.executeCommand("b"));
	}
	@Test
	public void executecomandTestmultiplecomand() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.RoverCaradd(0, 0, 'N');
		assertEquals("(0,4,N)",rover.executeCommand("ffrff"));
	}
	@Test
	public void executeplanetover() throws MarsRoverException {

		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.RoverCaradd(0, 0, 'N');
		assertEquals("(9,0,N)",rover.executeCommand("b"));
	}
}
