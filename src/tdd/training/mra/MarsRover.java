package tdd.training.mra;

import java.util.List;

public class MarsRover {

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private int rovx;
	private int rovy;
	private char orientation;
	
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.planetX=planetX;
		this.planetY=planetY;
		
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
	    
	
		return true;
		
	}

	
	
	
	
	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		int num=0;
		int start=0;
		int end =1;
		String part,nullex=null;
		
		num=commandString.length();
		
		for(int i=0;i!=num;i++)
		{
			if(num==1)
			{
			part=commandString;	
			}
			else
			{
		part = commandString.substring(start,end);
		start++;
		end++;
			}
		
		
		if(part.equals(" "))
		{
			 nullex="(0,0,N)";
			
		}
		else
		{
			if(part.equals("r"))
			{
				 nullex="("+getx()+","+gety()+","+"E"+")";
			
			}
			else
			{
				if(part.equals("l"))
				{
					 nullex="(0,0,W)";
					
				}
				else
				{
					if(part.equals("f"))
					{
						
						int x=getx();
						int y=gety();
						y++;
						sety(y);
						 nullex="("+x+","+y+","+getorientation()+")";
						
					}
					else
					{
						if(part.equals("b"))
						{
							int x=getx();
							int y=gety();
							x--;
							setx(x);
							nullex="("+getx()+","+gety()+","+getorientation()+")";
							
						}
					}
				}
			}
		}
		
		}
		
		return nullex;
	}
	
	
	
	
	
	public void RoverCaradd( int x,int y,char pos)
	{
		this.rovx=x;
		this.rovy=y;
		this.orientation=pos;
	}
	
	public int getx()
	{
		return this.rovx;
	}
	
	
	public int gety()
	{
		return rovy;
		
	}
	
	public void sety(int y)
	{
		 rovy=y;
		 if(rovy==-1)
		 {
			 rovy=9;
		 }
	}
	public void setx(int x)
	{
		
		 rovx=x;
		 if(rovx==-1)
		 {
			 rovx=9;
		 }
		 
		
	}
	public char getorientation()
	{
		return orientation;
		
	}
	public void setpos(char or)
	{
		 orientation=or;
		
	}
	
	

}
